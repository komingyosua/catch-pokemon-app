import * as React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils';
import SearchBar from './SearchBar';

describe('SearchBar', () => {
  const mockSetFindPokemon = jest.fn();
  const renderComponent = () => {
    return shallow(
      <SearchBar
        findPokemon="test"
        setFindPokemon={mockSetFindPokemon}
      />
    );
  };

  beforeEach(() => {
    localStorage.setItem('pokemons', JSON.stringify([]));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component SearchBar correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });

  describe('#handleOnChange', () => {
    it('should change find pokemon field and call setFindPokemon when invoked', () => {
      const mockFindPokemonValue = {
        target: {
          value: 'bulbasaur'
        }
      };
      const renderedComponent = renderComponent();

      const { onChange } = renderedComponent.props().children[0].props;
      onChange(mockFindPokemonValue);

      expect(mockSetFindPokemon).toHaveBeenCalledWith('bulbasaur');
    });
  });
});
