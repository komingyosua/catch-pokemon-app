import React from 'react';
import PropTypes from 'prop-types';
import {
  SearchWrapper,
  SearchInput
} from '../../Styled/SearchBar';

const SearchBar = (props) => {
  const { findPokemon, setFindPokemon } = props;
  const listOfPokemon = JSON.parse(localStorage.getItem('pokemons')) || [];

  const handleOnChange = (event) => {
    setFindPokemon(event.target.value);
  };

  return (
    <SearchWrapper>
      <SearchInput
        type="text"
        placeholder="Find your Pokemon ..."
        value={findPokemon}
        onChange={handleOnChange}
      />
      <span>
        Owned Pokemons:
        {' '}
        {listOfPokemon.length}
      </span>
    </SearchWrapper>
  );
};

SearchBar.propTypes = {
  findPokemon: PropTypes.string.isRequired,
  setFindPokemon: PropTypes.func.isRequired
};

export default SearchBar;
