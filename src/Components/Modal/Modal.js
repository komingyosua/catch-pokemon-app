import React, { useState } from 'react';
import { isEmpty } from 'lodash';
import { useHistory } from 'react-router-dom';
import {
  InfoModal,
  DetailModal,
  CloseButton,
  DescriptionModal,
  NameInput,
  SubmitButton, ErrorMessage
} from '../../Styled/Modal';
import { MessageConstants } from '../../Constants';

const Modal = (props) => {
  const {
    isShowModal,
    setIsShowModal,
    pokemonDetail,
    successIndicator,
  } = props;
  const history = useHistory();
  const {
    SUCCESS_CATCH_POKEMON,
    FAILED_CATCH_POKEMON,
    POKEMON_NAME_TAKEN
  } = MessageConstants;

  const [pokemonName, setPokemonName] = useState('');
  const [isError, setIsError] = useState(false);
  const listOfPokemon = JSON.parse(localStorage.getItem('pokemons')) || [];

  const handleCloseModal = () => {
    setIsShowModal(false);
  };

  const handleChangePokemonName = (event) => {
    const { target: { value } } = event;
    setPokemonName(value);
  };

  const handleSubmitPokemon = () => {
    const isNameAvailable = listOfPokemon.filter((pokemon) => pokemon.pokemonName.includes(pokemonName));
    if (isEmpty(isNameAvailable)) {
      const newPokemon = {
        pokemonName,
        ...pokemonDetail
      };
      listOfPokemon.push(newPokemon);
      localStorage.setItem('pokemons', JSON.stringify(listOfPokemon));
      return history.push('/my-pokemons');
    }
    setIsError(true);
    return null;
  };

  return (
    <>
      {isShowModal && (
        <InfoModal>
          <DetailModal>
            <CloseButton
              className="close-button"
              onClick={handleCloseModal}
            >
              X
            </CloseButton>
            <DescriptionModal>
              {!successIndicator ? SUCCESS_CATCH_POKEMON : FAILED_CATCH_POKEMON}
              {!successIndicator && (
                <>
                  <NameInput
                    value={pokemonName}
                    onChange={handleChangePokemonName}
                    placeholder="Insert a name"
                    className="input-pokemon-name"
                  />
                  <SubmitButton
                    onClick={handleSubmitPokemon}
                    className="submit-pokemon"
                  >
                    Submit
                  </SubmitButton>
                  <ErrorMessage className="modal-error-message">
                    {isError && POKEMON_NAME_TAKEN}
                  </ErrorMessage>
                </>
              )}
            </DescriptionModal>
          </DetailModal>
        </InfoModal>
      )}
    </>
  );
};

export default Modal;
