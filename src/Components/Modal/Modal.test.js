import * as React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils';
import Modal from './Modal';
import { mockPokemon } from '../../Fixtures/PokemonFixtures';

const mockPush = jest.fn();
jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: mockPush
  }),
}));
describe('Modal', () => {
  const mockSetIsShowModal = jest.fn();
  const renderComponent = () => {
    return shallow(
      <Modal
        isShowModal={true}
        setIsShowModal={mockSetIsShowModal}
        pokemonDetail={mockPokemon}
        successIndicator={false}
      />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component Modal correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });

    it('should render component Modal correctly when successIndicator is true and invoked', () => {
      const renderedComponent = shallow(
        <Modal
          isShowModal={true}
          setIsShowModal={mockSetIsShowModal}
          pokemonDetail={mockPokemon}
          successIndicator={true}
        />
      );

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });

  describe('#handleCloseModal', () => {
    it('should call handleOnClick when close button is clicked', () => {
      const renderedComponent = renderComponent();

      const { onClick } = renderedComponent.find('.close-button').get(0).props;
      onClick();

      expect(mockSetIsShowModal).toHaveBeenCalled();
    });
  });

  describe('#handleChangePokemonName', () => {
    it('should change value to bulbasaur when onChange is invoked', () => {
      const mockPokemonName = {
        target: {
          value: 'bulbasaur'
        }
      };
      const renderedComponent = renderComponent();

      const { onChange } = renderedComponent.find('.input-pokemon-name').get(0).props;
      onChange(mockPokemonName);
      renderedComponent.update();
      const { value } = renderedComponent.find('.input-pokemon-name').get(0).props;

      expect(value).toEqual('bulbasaur');
    });
  });

  describe('#handleSubmitPokemon', () => {
    it('should call push function when handleSubmitPokemon is invoked', () => {
      localStorage.setItem('pokemons', JSON.stringify([]));
      const renderedComponent = renderComponent();

      const { onClick } = renderedComponent.find('.submit-pokemon').get(0).props;
      onClick();

      expect(mockPush).toHaveBeenCalledWith('/my-pokemons');
    });

    it('should call setIsError and return error message when handleSubmitPokemon is invoked', () => {
      localStorage.setItem('pokemons', JSON.stringify([{ pokemonName: 'bulbasaur' }]));
      const renderedComponent = renderComponent();

      const { onClick } = renderedComponent.find('.submit-pokemon').get(0).props;
      onClick();
      const { children } = renderedComponent.find('.modal-error-message').get(0).props;

      expect(children).toEqual('The name is already taken!');
    });
  });
});
