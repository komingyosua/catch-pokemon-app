import React from 'react';
import { shallow } from 'enzyme';
import { PokemonCharacter } from '../index';
import { convertToSnapshot } from '../../Utils';
import { mockCharacter } from '../../Fixtures/PokemonFixtures';

const mockPush = jest.fn();
jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: mockPush
  }),
}));

describe('PokemonCharacter', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const renderComponent = () => {
    return shallow(
      <PokemonCharacter
        character={mockCharacter}
        removeable
      />
    );
  };

  describe('#render', () => {
    it('should render component PokemonCharacter correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });

    it('should render component PokemonCharacter correctly when image and pokemonName is not available', () => {
      const customMockCharacter = {
        name: 'Bulbasaur',
        sprites: {
          front_default: 'img'
        }
      };
      const renderedComponent = shallow(
        <PokemonCharacter
          character={customMockCharacter}
          removeable
        />
      );

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });

  describe('#handleRedirectToDetail', () => {
    it('should call push function when onClick is invoked', () => {
      const renderedComponent = renderComponent();

      const { onClick } = renderedComponent.props();
      onClick();

      expect(mockPush).toHaveBeenCalledWith('/pokemons/Bulbasaur');
    });
  });

  describe('#handleDeletePokemon', () => {
    it('should call push function when onClick is invoked', () => {
      const mockEvent = {
        stopPropagation: jest.fn()
      };
      localStorage.setItem('pokemons', JSON.stringify([{ pokemonName: 'Test' }]));
      const renderedComponent = renderComponent();

      const { onClick } = renderedComponent.find('.delete-pokemon').get(0).props;
      onClick(mockEvent);

      expect(mockPush).toHaveBeenCalledWith('/my-pokemons');
    });
  });
});
