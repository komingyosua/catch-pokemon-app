import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { isEmpty } from 'lodash';
import {
  CharacterWrapper,
  Character,
  CharacterName, DeleteButton
} from '../../Styled/PokemonCharacter';

const PokemonCharacter = (props) => {
  const {
    removeable,
    character: {
      name,
      image,
      pokemonName,
      sprites
    }
  } = props;
  const history = useHistory();

  const handleDeletePokemon = (event) => {
    event.stopPropagation();
    const pokemons = JSON.parse(localStorage.getItem('pokemons'));
    const filteredPokemons = pokemons.filter((pokemon) => pokemon.pokemonName !== pokemonName);
    localStorage.setItem('pokemons', JSON.stringify(filteredPokemons));
    return history.push('/my-pokemons');
  };

  const handleRedirectToDetail = () => {
    history.push(`/pokemons/${name}`);
  };

  return (
    <CharacterWrapper onClick={handleRedirectToDetail}>
      <Character>
        {removeable && (
          <DeleteButton
            className="delete-pokemon"
            onClick={handleDeletePokemon}
          >
            X
          </DeleteButton>
        )}
        <img alt="" src={image || sprites.front_default} />
        <CharacterName>
          {!isEmpty(pokemonName) ? pokemonName : name}
        </CharacterName>
      </Character>
    </CharacterWrapper>
  );
};

PokemonCharacter.propTypes = {
  character: PropTypes.object.isRequired
};

export default PokemonCharacter;
