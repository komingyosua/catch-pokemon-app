import * as React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils';
import Navbar from './Navbar';

describe('Navbar', () => {
  const renderComponent = () => {
    return shallow(
      <Navbar />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component Navbar correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
