import React from 'react';
import { NavLink } from 'react-router-dom';
import { Navigation } from '../../Styled/Navbar';

const Navbar = () => {
  return (
    <Navigation>
      <ul>
        <li>
          <NavLink to="/pokemons">Pokemons</NavLink>
          <NavLink to="/my-pokemons">My Pokemons</NavLink>
        </li>
      </ul>
    </Navigation>
  );
};

export default Navbar;
