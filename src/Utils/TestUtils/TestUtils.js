import EnzymeToJson from 'enzyme-to-json';

export const convertToSnapshot = (wrapper) => {
  return EnzymeToJson(wrapper);
};
