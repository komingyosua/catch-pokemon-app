import React from 'react';
import ReactDOM from 'react-dom';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache
} from '@apollo/client';
import styled from '@emotion/styled';
import { BrowserRouter as Router } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import AppRouter from './Routers/AppRouter/AppRouter';
import './index.scss';
import { pokemonLogo } from './Assets';

const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});

const Container = styled.div`
    width: 100%;
    min-height: 100vh;
    background-image: linear-gradient(to left, #fbc2eb 0%, #a6c1ee 100%);
    font-family: 'Noto Sans JP', sans-serif;
  `;

const LogoWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 30px 0 50px 0;
  `;

ReactDOM.render(
  <ApolloProvider client={client}>
    <Container>
      <LogoWrapper>
        <img alt="" src={pokemonLogo} width="275" />
      </LogoWrapper>
      <Router>
        <AppRouter />
      </Router>
    </Container>
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
