import { GET_POKEMON_DETAIL } from '../Queries';

export const mockPokemon = {
  abilities: [
    {
      ability: {
        name: 'overgrow'
      }
    }
  ],
  id: 1,
  name: 'bulbasaur',
  sprites: {
    front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png'
  },
  types: [
    {
      type: {
        name: 'grass'
      }
    }
  ]
};

export const mockCharacter = {
  name: 'Bulbasaur',
  image: 'img',
  pokemonName: 'Test',
  sprites: {
    front_default: 'img'
  },
  types: [
    {
      type: {
        name: 'test'
      }
    }
  ],
  abilities: [
    {
      ability: {
        name: 'test'
      }
    }
  ],
  moves: [
    {
      move: {
        name: 'test'
      }
    }
  ]
};

export const mockPokemons = [{
  name: 'Bulbasaur',
  image: 'img',
  pokemonName: 'Test',
  sprites: {
    front_default: 'img'
  }
}];

export const mockDetail = [
  {
    request: {
      query: GET_POKEMON_DETAIL,
      variables: {
        name: 'bulbasaur',
      },
    },
    result: {
      data: {
        pokemon: {
          abilities: [
            {
              ability: {
                name: 'overgrow'
              }
            }
          ],
          id: 1,
          name: 'bulbasaur',
          sprites: {
            front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png'
          },
          types: [
            {
              type: {
                name: 'grass'
              }
            }
          ]
        },
      }
    },
  },
];
