import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import { isEmpty } from 'lodash';
import { GET_POKEMON_DETAIL } from '../../Queries';
import { ContentWrapper } from '../../Styled/GlobalStyle';
import Modal from '../../Components/Modal/Modal';
import {
  DetailWrapper,
  CharacterName,
  CharacterDetail,
  ShowMoveButton,
  Moves,
  CatchButton,
  BlockDetail
} from '../../Styled/PokemonDetail';

const PokemonDetail = (props) => {
  const { match: { params: { name } } } = props;
  const gqlOptions = {
    variables: {
      name
    }
  };
  const { data } = useQuery(GET_POKEMON_DETAIL, gqlOptions);
  const [pokemonDetail, setPokemonDetail] = useState({});
  const [isShowMoves, setIsShowMoves] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [successIndicator, setSuccessIndicator] = useState(false);

  useEffect(() => {
    if (data) {
      const { pokemon } = data;
      setPokemonDetail(pokemon);
    }
  }, [data]);

  const isSuccessChance = () => {
    return Math.random() > 0.5;
  };

  const handleCatchPokemon = () => {
    setIsShowModal(true);
    if (isSuccessChance()) {
      return setSuccessIndicator(true);
    }
    return setSuccessIndicator(false);
  };

  const handleShowMoves = () => {
    setIsShowMoves(!isShowMoves);
  };

  const renderModal = () => (
    <Modal
      isShowModal={isShowModal}
      setIsShowModal={setIsShowModal}
      pokemonDetail={pokemonDetail}
      successIndicator={successIndicator}
      className="pokemon-detail-modal"
    />
  );

  const renderTypes = () => (
    <BlockDetail>
      Type:
      {pokemonDetail.types.map((type) => (
        <CharacterDetail
          key={type.type.name.toString()}
        >
          {type.type.name}
        </CharacterDetail>
      ))}
    </BlockDetail>
  );

  const renderAbilities = () => (
    <BlockDetail>
      Abilities:
      {pokemonDetail.abilities.map((ability) => (
        <CharacterDetail
          key={ability.ability.name.toString()}
        >
          {ability.ability.name}
        </CharacterDetail>
      ))}
    </BlockDetail>
  );

  const renderShowMoveButton = () => (
    <ShowMoveButton
      className="show-more-button"
      onClick={handleShowMoves}
    >
      {!isShowMoves ? 'Show Moves' : 'Hide Moves'}
    </ShowMoveButton>
  );

  const renderMoves = () => (
    <BlockDetail>
      {isShowMoves && pokemonDetail.moves.map((move) => (
        <Moves
          key={move.move.name.toString()}
        >
          {move.move.name}
        </Moves>
      ))}
    </BlockDetail>
  );

  const renderCatchButton = () => (
    <CatchButton
      type="button"
      onClick={handleCatchPokemon}
      className="catch-button"
    >
      Catch Pokemon!
    </CatchButton>
  );

  return (
    <ContentWrapper>
      {renderModal()}
      {!isEmpty(pokemonDetail) && (
        <DetailWrapper>
          <img alt="" src={pokemonDetail.sprites.front_default} width="150" />
          <CharacterName>{pokemonDetail.name}</CharacterName>
          {renderTypes()}
          {renderAbilities()}
          {renderShowMoveButton()}
          {renderMoves()}
          {renderCatchButton()}
        </DetailWrapper>
      )}
    </ContentWrapper>
  );
};

export default React.memo(PokemonDetail);
