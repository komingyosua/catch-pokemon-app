import * as React from 'react';
import { mount } from 'enzyme';
import { MockedProvider } from '@apollo/client/testing';
import PokemonDetail from './PokemonDetail';
import { convertToSnapshot } from '../../Utils';
import { mockCharacter, mockDetail } from '../../Fixtures/PokemonFixtures';

describe('PokemonDetail', () => {
  const mockParams = {
    params: {
      name: 'Bulbasaur'
    }
  };
  const renderComponent = () => {
    return mount(
      <MockedProvider
        mocks={mockDetail}
        addTypename={false}
      >
        <PokemonDetail
          match={mockParams}
        />
      </MockedProvider>
    );
  };

  const handleMockRandom = (value) => {
    jest
      .spyOn(global.Math, 'random')
      .mockReturnValue(value);
  };

  const handleMockState = (value) => {
    const realUseState = React.useState;
    jest
      .spyOn(React, 'useState')
      .mockImplementationOnce(() => realUseState(value));
  };

  beforeEach(() => {
    jest.restoreAllMocks();
  });

  describe('#handleCatchPokemon', () => {
    it('should call handleCatchPokemon when catch button is clicked', async () => {
      handleMockRandom(0.6);
      handleMockState(mockCharacter);
      const renderedComponent = renderComponent();

      const { props: { onClick } } = renderedComponent.find('.catch-button').get(0);
      onClick();
      renderedComponent.update();
      const { successIndicator, isShowModal } = renderedComponent.find('.pokemon-detail-modal').props();

      expect(successIndicator).toBeTruthy();
      expect(isShowModal).toBeTruthy();
    });

    it('should call handleCatchPokemon when catch success chance is false and button is clicked', async () => {
      handleMockRandom(0.4);
      handleMockState(mockCharacter);
      const renderedComponent = renderComponent();

      const { props: { onClick } } = renderedComponent.find('.catch-button').get(0);
      onClick();
      renderedComponent.update();
      const { successIndicator, isShowModal } = renderedComponent.find('.pokemon-detail-modal').props();

      expect(successIndicator).toBeFalsy();
      expect(isShowModal).toBeTruthy();
    });
  });

  describe('#handleShowMoves', () => {
    it('should call handleShowMoves when show moves button is clicked', async () => {
      handleMockState(mockCharacter);
      const renderedComponent = renderComponent();

      const { props: { onClick } } = renderedComponent.find('.show-more-button').get(0);
      onClick();
      renderedComponent.update();
      renderedComponent.render();
      const ShowMoreButtonText = renderedComponent.find('.show-more-button').get(0).props.children;
      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
      expect(ShowMoreButtonText).toEqual('Hide Moves');
    });
  });
});
