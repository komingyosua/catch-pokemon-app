import React, { useEffect, useState } from 'react';
import { isEmpty } from 'lodash';
import { PokemonCharacter } from '../../Components';
import { ContentWrapper } from '../../Styled/GlobalStyle';

const MyPokemonList = () => {
  const [listOfPokemon, setListOfPokemon] = useState([]);

  useEffect(() => {
    const pokemons = JSON.parse(localStorage.getItem('pokemons'));
    if (!isEmpty(pokemons)) {
      setListOfPokemon(pokemons);
    }
  }, []);

  return (
    <ContentWrapper>
      {!isEmpty(listOfPokemon) && listOfPokemon.map((pokemon) => (
        <PokemonCharacter
          key={pokemon.name.toString()}
          character={pokemon}
          removeable
        />
      ))}
    </ContentWrapper>
  );
};

export default MyPokemonList;
