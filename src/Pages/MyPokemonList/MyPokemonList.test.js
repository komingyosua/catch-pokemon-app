import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { convertToSnapshot } from '../../Utils';
import MyPokemonList from './MyPokemonList';
import { mockPokemons } from '../../Fixtures/PokemonFixtures';

describe('MyPokemonList', () => {
  const renderComponent = () => {
    return shallow(
      <MyPokemonList />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component MyPokemonList correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });

    it('should render children false in MyPokemonList when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = renderedComponent.props().children;

      expect(actualComponent)
        .toBe(false);
    });

    it('should render component MyPokemonList with data correctly when invoked', () => {
      localStorage.setItem('pokemons', JSON.stringify(mockPokemons));
      const renderedComponent = mount(
        <MyPokemonList />
      );
      jest
        .spyOn(React, 'useEffect')
        .mockImplementation((fn) => fn());

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
