import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { isEmpty } from 'lodash';
import { GET_POKEMONS } from '../../Queries';
import { PokemonCharacter, SearchBar } from '../../Components';
import { ContentWrapper } from '../../Styled/GlobalStyle';

const PokemonList = () => {
  const gqlOptions = {
    variables: {
      limit: 50,
      offset: 0,
    },
    fetchPolicy: 'cache-and-network'
  };
  const { data } = useQuery(GET_POKEMONS, gqlOptions);
  const [pokemonList, setPokemonList] = useState([]);
  const [findPokemon, setFindPokemon] = useState('');

  useEffect(() => {
    if (data) {
      const { pokemons: { results } } = data;
      setPokemonList(results);
    }
  }, [data]);

  const filteredPokemons = () => {
    const searchedPokemon = findPokemon.toLowerCase();

    return pokemonList.filter((pokemon) => pokemon.name.includes(searchedPokemon));
  };

  return (
    <ContentWrapper>
      <SearchBar
        findPokemon={findPokemon}
        setFindPokemon={setFindPokemon}
      />
      {!isEmpty(filteredPokemons()) && filteredPokemons().map((pokemon) => (
        <PokemonCharacter
          key={pokemon.name.toString()}
          character={pokemon}
        />
      ))}
    </ContentWrapper>
  );
};

export default React.memo(PokemonList);
