import * as React from 'react';
import { mount } from 'enzyme';
import { MockedProvider } from '@apollo/client/testing';
import wait from 'waait';
import { convertToSnapshot } from '../../Utils';
import PokemonList from './PokemonList';
import { mockDetail, mockPokemons } from '../../Fixtures/PokemonFixtures';

describe('PokemonList', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const handleMockState = (value) => {
    const realUseState = React.useState;
    jest
      .spyOn(React, 'useState')
      .mockImplementationOnce(() => realUseState(value));
  };

  describe('#render', () => {
    it('should render component PokemonList with data correctly when invoked', async () => {
      localStorage.setItem('pokemons', JSON.stringify(mockPokemons));
      handleMockState(mockPokemons);
      const renderedComponent = mount(
        <MockedProvider
          mocks={mockDetail}
          addTypename={false}
        >
          <PokemonList />
        </MockedProvider>
      );

      await wait(0);
      await renderedComponent.update();
      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
