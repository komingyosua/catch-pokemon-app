import React from 'react';
import {
  Switch,
  Redirect
} from 'react-router-dom';
import { Route } from '../Route/Route';
import {
  PokemonList,
  PokemonDetail,
  MyPokemonList
} from '../../Pages';
import { Navbar } from '../../Components';

const AppRouter = () => {
  return (
    <>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Redirect to="/pokemons" />
        </Route>
        <Route path="/pokemons" exact component={PokemonList} />
        <Route path="/pokemons/:name" component={PokemonDetail} strict key="name" />
        <Route path="/my-pokemons" component={MyPokemonList} />
      </Switch>
    </>
  );
};

export default AppRouter;
