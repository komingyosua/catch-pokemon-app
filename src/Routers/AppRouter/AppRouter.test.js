import * as React from 'react';
import { mount } from 'enzyme';
import { MockedProvider } from '@apollo/client/testing';
import { MemoryRouter } from 'react-router-dom';
import { convertToSnapshot } from '../../Utils';
import AppRouter from './AppRouter';

describe('AppRouter', () => {
  const renderComponent = () => {
    return mount(
      <MockedProvider>
        <MemoryRouter initialEntries={['/']} keyLength={0}>
          <AppRouter />
        </MemoryRouter>
      </MockedProvider>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component AppRouter correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
