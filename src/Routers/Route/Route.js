import React from 'react';
import PropTypes from 'prop-types';
import { Route as RouterRoute } from 'react-router-dom';

export const Route = ({ component: Component, ...rest }) => (
  <RouterRoute
    {...rest}
    component={(props) => (
      <Component {...props} />
    )}
  />
);

Route.defaultProps = {
  component: {}
};

Route.propTypes = {
  component: PropTypes.any
};

export default Route;
