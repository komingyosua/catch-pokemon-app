import * as React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils';
import Route from './Route';
import { PokemonDetail } from '../../Pages';

describe('Route', () => {
  const renderComponent = () => {
    return shallow(
      <Route path="/pokemons" component={PokemonDetail} />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component Route correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
