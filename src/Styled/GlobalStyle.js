import styled from '@emotion/styled';
import { mediaQuery } from '../Constants';

export const ContentWrapper = styled.div`
    padding: 30px;
    ${mediaQuery('small')} {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      flex-wrap: wrap;
    }
  `;
