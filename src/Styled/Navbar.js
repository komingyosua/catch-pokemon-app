import styled from '@emotion/styled';

export const Navigation = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 30px 0 50px 0;
    
    ul {
      list-style: none; 
      margin: 0;
      padding: 0;
    }
    
    ul li a{
      padding: 8px 15px;
      color: #555;
      text-decoration: none;
      font-size: 18px;
    }
    
    .active {
      background: #FFF;
      border-radius: 100px;
    }
  `;
