import styled from '@emotion/styled';
import { mediaQuery } from '../Constants';

export const CharacterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 15px 0;
    padding: 15px;
    font-family: 'Noto Sans JP', sans-serif;
    text-transform: capitalize;
    cursor: pointer;
    
    ${mediaQuery('xsmall')} {
      width: 100%;
    }
    
    ${mediaQuery('small')} {
      width: 50%;
    }
    
    ${mediaQuery('medium')} {
      width: 33.3%;
    }
    
    ${mediaQuery('large')} {
      width: 20%;
    }
  `;

export const Character = styled.div`
    ${mediaQuery('xsmall')} {
      width: 180px;
      height: 200px;
      background: #fafafa;
      text-align: center;
      position: relative;
      border-radius: 20px;
      padding: 10px 0;
     
      img {
        width: 120px;
      }
    }
  `;

export const CharacterName = styled.h3`
    color: #555;
    text-decoration: none;
    font-weight: 500;
    padding: 0px 0;
  `;

export const DeleteButton = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 100px;
    position: absolute;
    font-size: 18px;
    background: #FFF;
    text-align: center;
    color: #555;
    text-decoration: none;
    font-weight: 500;
    padding: 5px 0;
    color: red;
    top: -15px;
    left: -15px;
    box-shadow: 0px 1px 3px rgba(0,0,0,0.3);
    cursor: pointer;
  `;
