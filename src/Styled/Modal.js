import styled from '@emotion/styled';

export const InfoModal = styled.div`
    width: 100%;
    min-height: 116.5vh;
    position: absolute;
    background: rgba(0,0,0,0.5);
    display: flex;
    align-items: center;
    justify-content: center;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
  `;

export const DetailModal = styled.div`
    width: 350px;
    height: 250px;
    background: #FFF;
    border-radius: 20px;
    padding: 15px 25px;
  `;

export const CloseButton = styled.div`
    cursor: pointer;
    text-align: right;
  `;

export const DescriptionModal = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 100%;
    height: 100%;
  `;

export const NameInput = styled.input`
    border: 0;
    width: 150px;
    height: 35px;
    border: 1px solid #555;
    border-radius: 10px;
    margin-top: 15px;
    padding: 15px;
    outline: 0;
  `;

export const SubmitButton = styled.button`
    background: #555;
    color: #FFF;
    border: 0;
    padding: 8px 15px;
    margin: 15px 0;
    border-radius: 10px;
  `;

export const ErrorMessage = styled.div`
  `;
