import styled from '@emotion/styled';
import { mediaQuery } from '../Constants';

export const DetailWrapper = styled.div`
  width: 100%;
  background: #FFF;
  border-radius: 20px;
  padding: 30px;
  text-align: center;
`;

export const CharacterName = styled.h3`
  color: #555;
  text-decoration: none;
  font-weight: 500;
  text-transform: Capitalize;
  margin-bottom: 15px;
  text-align: center;
`;

export const CharacterDetail = styled.div`
  ${mediaQuery('xsmall')} {
    width: auto;
    display: block;
  }
  ${mediaQuery('small')} {
    display: inline-block;
  }
  color: #555;
  text-decoration: none;
  font-weight: 500;
  text-transform: Capitalize;
  display: inline-block;
  background: #EEE;
  padding: 5px 15px;
  border-radius: 100px;
  font-size: 13px;
  margin: 8px 5px;
`;

export const CatchButton = styled.button`
  width: 100%;
  height: 50px;
  background-image: linear-gradient(to left, #fbc2eb 0%, #a6c1ee 100%);
  border: 0;
  border-radius: 100px;
  color: #FAFAFA;
  font-weight: bold;
  margin-top: 15px;
  cursor: pointer;
  outline: 0;
`;

export const ShowMoveButton = styled.div`
  padding: 15px 0;
  cursor: pointer;
  color: #2196F3;
`;

export const Moves = styled.div`
  text-transform: capitalize;
  line-height: 28px;
`;

export const BlockDetail = styled.div`
  display: block;
`;
