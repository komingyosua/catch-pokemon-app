import styled from '@emotion/styled';

export const SearchWrapper = styled.div`
    display: flex;
    width: 100%;
    justify-content: center;
    flex-direction: column;
    padding: 15px 15px 30px;
    
    span {
      text-align: right;
      color: #333;
      margin: 15px 0 0 0;
      font-weight: 500;
    }
  `;

export const SearchInput = styled.input`
    padding: 25px;
    border-radius: 10px;
    border: 0;
    box-shadow: none;
    font-size: 18px;
    width: 100%;
    outline: 0;
  `;
