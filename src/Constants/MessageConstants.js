const MessageConstants = {
  SUCCESS_CATCH_POKEMON: 'The pokemon successfully catched!',
  FAILED_CATCH_POKEMON: 'Oh no! let\'s do it again later',
  POKEMON_NAME_TAKEN: 'The name is already taken!'
};

export default MessageConstants;
