const breakpoints = {
  xsmall: 320,
  small: 500,
  medium: 768,
  large: 1200
};

export const mediaQuery = (sizeName) => {
  const breakpointArray = Object.keys(breakpoints).map((key) => [key, breakpoints[key]]);

  const [result] = breakpointArray.reduce((initialValue, [name, size]) => {
    if (sizeName === name) {
      return [...initialValue, `@media (min-width: ${size}px)`];
    }
    return initialValue;
  }, []);

  return result;
};
