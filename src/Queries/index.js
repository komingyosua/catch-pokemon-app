import {
  GET_POKEMONS,
  GET_POKEMON_DETAIL
} from './PokemonQueries';

export {
  GET_POKEMONS,
  GET_POKEMON_DETAIL
};
